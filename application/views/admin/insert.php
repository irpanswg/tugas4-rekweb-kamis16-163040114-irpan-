<h3 class="text-center">Form penambahan data produk</h3>
<form action="add" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleInputEmail1">ID Produk</label>
    <input type="text" class="form-control" id="exampleInputId"  placeholder="Masukan ID Produk" name="id">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Nama Produk</label>
    <input type="text" class="form-control" id="exampleInputNama"  placeholder="Masukan Nama Produk" name="nama_produk">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Deskripsi</label>
    <input style="width: 100%; height: 100px;" type="text" class="form-control" id="exampleInputDeskripsi" placeholder="Masukan Deskripsi" name="deskripsi">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1">Harga</label>
    <input type="number" class="form-control" id="exampleInputHarga" placeholder="Masukan Harga" name="harga">
  </div>
  <div class="form-group form-check">
    <label for="exampleInputEmail1">Jumlah Barang</label>
    <input type="number" class="form-control" id="exampleInputStok" placeholder="Masukan jumlah stok barang" name="stok">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Gambar</label>
    <input type="file" class="form-control-file" id="exampleFormGambar" name="gambar">
    <small>Ukuran size 5MB</small>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label" for="exampleCheck1">Kategori </label>&nbsp;
    <select name="kategori">
  <option value="1">Remote Control</option>
  <option value="2">Boneka</option>
  <option value="3">Action Figure</option>
</select>
  </div>
  
  <button type="submit" class="btn btn-success" name="submit">Tambah</button>
  <div><br></div>
</form>