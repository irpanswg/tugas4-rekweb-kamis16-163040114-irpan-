<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Keranjang_model extends CI_Model {

	public function get_produk_all()
	{
		$query = $this->db->get('tbl_produk');
		return $query->result_array();
	}
	
	public function get_produk_kategori($kategori)
	{
		if($kategori>0)
			{
				$this->db->where('kategori',$kategori);
			}
		$query = $this->db->get('tbl_produk');
		return $query->result_array();
	}
	
	public function get_kategori_all()
	{
		$query = $this->db->get('tbl_kategori');
		return $query->result_array();
	}
	
	public  function get_produk_id($id)
	{
		$this->db->select('tbl_produk.*,nama_kategori');
		$this->db->from('tbl_produk');
		$this->db->join('tbl_kategori', 'kategori=tbl_kategori.id','left');
   		$this->db->where('id_produk',$id);
        return $this->db->get();
    }

    public  function get_orderan()
	{
		// $query = $this->db->get('tbl_pelanggan');
		$this->db->select('*');
		$this->db->from('tbl_order');
		$this->db->join('tbl_detail_order', 'tbl_detail_order.order_id=tbl_order.id');
		$this->db->join('tbl_pelanggan', 'tbl_pelanggan.id=tbl_order.id');
   		// $this->db->where('id_produk',$id);
		$query = $this->db->get();
		return $query->result_array();
    }	
	
	public function tambah_pelanggan($data)
	{
		$this->db->insert('tbl_pelanggan', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_order($data)
	{
		$this->db->insert('tbl_order', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_detail_order($data)
	{
		$this->db->insert('tbl_detail_order', $data);
	}
	public function insertProduk($prk)
	{
		$this->db->insert('tbl_produk', $prk);
		return $this->db->affected_rows();
	}
	public function deleteProduk($id)
	{
		$this->db->where("id_produk", $id);
		return $this->db->delete("tbl_produk");
	}
	public function get_produk_by($id)
	{
		return $this->db->get_where('tbl_produk',['id_produk' => $id])->result();
	}
	public function updateProduk($prk, $id)
	{
		$this->db->where('id_produk', $id);
		return $this->db->update('tbl_produk', $prk);
	}
}
?>