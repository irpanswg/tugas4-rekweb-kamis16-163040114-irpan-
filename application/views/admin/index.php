<h2>Daftar Produk</h2>
<br>
<table class="table">
<tr id= "main_heading">
<th class="text-center">No</th>
<th class="text-center" width="10%">ID Produk</th>
<th class="text-center" width="10%">Gambar</th>
<th class="text-center" width="15%">Nama Produk</th>
<th class="text-center" width="15%">Deskripsi</th>
<th class="text-center" width="10%">Harga</th>
<th class="text-center" width="10%">Jumlah Barang</th>
<th class="text-center" width="10%">Kategori</th>

<th class="text-center" width="15%">Aksi</th>
</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($produk as $item):
?>

<tr>
<td class="text-center"><?= $i++; ?></td>
<td class="text-center"><?= $item->id_produk ?></td>
<td><img class="img-responsive" src="<?php echo base_url() . 'assets/images/'.$item->gambar ?>"/></td>
<td><?= $item->nama_produk ?></td>
<td><?= $item->deskripsi ?></td>
<td class="text-center"><h5>Rp.<?= $item->harga ?></h5></td>
<td class="text-center"><?= $item->stok ?></td>
<td class="text-center"><?= $item->kategori ?></td>
<td class="text-center">
	<a href="<?php echo site_url('page/update/'.$item->id_produk) ?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-edit"></i></a>
	<a href="<?php echo site_url('page/hapus/'.$item->id_produk) ?>" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;
</td>
<?php endforeach; ?>
</tr>

</table>