<h2>Daftar Orderan Pelanggan</h2>
<br>
<table class="table">
<tr id= "main_heading">
<th class="text-center">No</th>
<th class="text-center" width="15%">ID Pelanggan</th>
<th class="text-center" width="10%">Nama Pelanggan</th>
<th class="text-center" width="15%">Email</th>
<th class="text-center" width="15%">Alamat</th>
<th class="text-center" width="10%">telp</th>
<th class="text-center" width="10%">Nama Produk</th>
<th class="text-center" width="10%">Jumlah Barang</th>
<th class="text-center" width="10%">Total</th>

</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($produk as $key):

?>

<tr>
<td class="text-center"><?= $i++; ?></td>
<td class="text-center"><?= "CS".$key->id ?></td>
<td class="text-center"><?= $key->nama ?></td>
<td class="text-center"><?= $key->email ?></td>
<td class="text-center"><?= $key->alamat ?></td>
<td class="text-center"><?= $key->telp ?></td>
<td class="text-center"><?= $key->nama_produk ?></td>
<td class="text-center"><?= $key->stok ?></td>
<td class="text-center"><?= $key->harga * $key->stok ?></td>
<?php endforeach ?>
</tr>
</table>